﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using webSysdatecback.EntityFrameworkCore;
using webSysdatecback.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace webSysdatecback.Web.Tests
{
    [DependsOn(
        typeof(webSysdatecbackWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class webSysdatecbackWebTestModule : AbpModule
    {
        public webSysdatecbackWebTestModule(webSysdatecbackEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(webSysdatecbackWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(webSysdatecbackWebMvcModule).Assembly);
        }
    }
}