﻿using System.Threading.Tasks;
using webSysdatecback.Models.TokenAuth;
using webSysdatecback.Web.Controllers;
using Shouldly;
using Xunit;

namespace webSysdatecback.Web.Tests.Controllers
{
    public class HomeController_Tests: webSysdatecbackWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}