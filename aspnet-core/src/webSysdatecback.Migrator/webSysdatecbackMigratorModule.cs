using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using webSysdatecback.Configuration;
using webSysdatecback.EntityFrameworkCore;
using webSysdatecback.Migrator.DependencyInjection;

namespace webSysdatecback.Migrator
{
    [DependsOn(typeof(webSysdatecbackEntityFrameworkModule))]
    public class webSysdatecbackMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public webSysdatecbackMigratorModule(webSysdatecbackEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(webSysdatecbackMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                webSysdatecbackConsts.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(webSysdatecbackMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
