﻿namespace webSysdatecback
{
    public class webSysdatecbackConsts
    {
        public const string LocalizationSourceName = "webSysdatecback";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
