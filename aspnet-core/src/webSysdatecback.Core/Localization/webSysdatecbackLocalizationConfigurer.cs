﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace webSysdatecback.Localization
{
    public static class webSysdatecbackLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(webSysdatecbackConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(webSysdatecbackLocalizationConfigurer).GetAssembly(),
                        "webSysdatecback.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
