﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class SolucionesEntity: FullAuditedEntity<long>
    {
        public string nombre { get; set; }
        public string descripcion { get; set; }
        [ForeignKey("IdCategoria")]
        public CategoriaCotizadorEntity categoria { get; set; }
        public long IdCategoria { get; set; }
    }
}
