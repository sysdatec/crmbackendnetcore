﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class ComentariosPostEntity: FullAuditedEntity<long>
    {
        public string cometario { get; set; }
        [ForeignKey("IdBlog")]
        public BlogEntity blog { get; set; }
        public long IdBlog { get; set; }
        public string usuario { get; set; }
    }
}
