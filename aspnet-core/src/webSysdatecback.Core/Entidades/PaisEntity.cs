﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class PaisEntity: FullAuditedEntity<long>
    {
        public string nombrePais { get; set; }
        public string codigo { get; set; }
    }
}
