﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class PlanesSolucioneEntity: FullAuditedEntity<long>
    {
        public int limiteUsuarios { get; set; }
        [ForeignKey("IdSolucion")]
        public SolucionesEntity soluciones { get; set; }
        public long IdSolucion { get; set; }
        public double valor { get; set; }
        public string nivel { get; set; }
    }
}
