﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class TransaccionesPaypalEntity : FullAuditedEntity<long>
    {
        public double precio { get; set; }
        public string detalle { get; set; }
        [ForeignKey("IdPersona")]
        public PersonasEntity persona { get; set; }
        public long IdPersona { get; set; }
        public string estado { get; set; }

    }
}
