﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class SolicitudesEntity: FullAuditedEntity<long>
    {
        public string nombres { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
        public string empresa { get; set; }

        [ForeignKey("IdPais")]
        public PaisEntity pais { get; set; }
        public long IdPais { get; set; }
    }
}
