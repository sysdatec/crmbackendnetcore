﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class AlmacenamientoAdicionalEntity: FullAuditedEntity<long>
    {
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string capacidad { get; set; }
        public double valor { get; set; }
        public string tipoAlmacenamiento { get; set; }
    }
}
