﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using webSysdatecback.Authorization.Users;

namespace webSysdatecback.Entidades
{
    public class BlogEntity : FullAuditedEntity<long>
    {
        public string titulo { get; set; }
        public string contenido { get; set; }
        public string urlContenido { get; set; }
        public string autor { get; set; }
        public string tipo { get; set; }
    }
}
