﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class CategoriaEntity: FullAuditedEntity
    {
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string modulo { get; set; }
    }
}
