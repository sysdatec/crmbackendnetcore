﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class CiudadEntity: FullAuditedEntity<long>
    {
        public string nombreCiudad { get; set; }
        public string codigo { get; set; }

        [ForeignKey("IdDepartamento")]
        public DepartamentoEntity departamentoEntity{ get; set; }
        public long IdDepartamento { get; set; }
    }
}
