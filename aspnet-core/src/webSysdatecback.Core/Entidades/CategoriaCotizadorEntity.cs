﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class CategoriaCotizadorEntity: FullAuditedEntity<long>
    {
        public string titulo { get; set; }
        public string descripcion { get; set; }
    }
}
