﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class DepartamentoEntity: FullAuditedEntity<long>
    {
        public string nombreDepartamento { get; set; }
        public string codigo { get; set; }

        [ForeignKey("IdPais")]
        public PaisEntity pais { get; set; }
        public long IdPais { get; set; }

    }
}
