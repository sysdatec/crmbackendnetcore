﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class CapacitacionesEntity : FullAuditedEntity<long>
    {
        public string titulo { get; set; }

        [ForeignKey("IdCategoria")]
        public CategoriaEntity categoria { get; set; }
        public int IdCategoria { get; set; }
        public string descripcion { get; set; }
        public string pathVideo { get; set; }
        public string url { get; set; }
    }
}
