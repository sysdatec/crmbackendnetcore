﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class LicenciaEntity: FullAuditedEntity<long>
    {
        public string titulo { get; set; }
        [ForeignKey("IdSolucion")]
        public SolucionesEntity solucion { get; set; }
        public long IdSolucion { get; set; }
        public double valor { get; set; }

    }
}
