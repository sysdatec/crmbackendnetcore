﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class Conceptosentity : FullAuditedEntity<long>
    {
        public string titulo { get; set; }
        public string referenciaProducto { get; set; }
    }
}
