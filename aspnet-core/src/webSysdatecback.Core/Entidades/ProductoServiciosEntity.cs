﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class ProductoServiciosEntity: FullAuditedEntity<long>
    {
        [ForeignKey("IdPersona")]
        public PersonasEntity persona { get; set; }
        public long IdPersona { get; set; }
        [ForeignKey("IdConcepto")]
        public Conceptosentity conceptos { get; set; }
        public long IdConcepto { get; set; }
        public double valor { get; set; }
    }
}
