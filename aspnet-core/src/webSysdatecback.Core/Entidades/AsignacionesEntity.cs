﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace webSysdatecback.Entidades
{
    public class AsignacionesEntity : FullAuditedEntity<long>
    {
        public string nombre { get; set; }
        public string codigo { get; set; }
        [ForeignKey("idConcepto")]
        public Conceptosentity concepto { get; set; }
        public long idConcepto { get; set; }
        public string descripcion { get; set; }
        public double valor { get; set; }
        public string estado { get; set; }
    }
}
