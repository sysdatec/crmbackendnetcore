﻿using Abp.MultiTenancy;
using webSysdatecback.Authorization.Users;

namespace webSysdatecback.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
