﻿using Abp.Authorization;
using webSysdatecback.Authorization.Roles;
using webSysdatecback.Authorization.Users;

namespace webSysdatecback.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
