﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using webSysdatecback.Authorization.Roles;
using webSysdatecback.Authorization.Users;
using webSysdatecback.MultiTenancy;
using webSysdatecback.Entidades;

namespace webSysdatecback.EntityFrameworkCore
{
    public class webSysdatecbackDbContext : AbpZeroDbContext<Tenant, Role, User, webSysdatecbackDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public webSysdatecbackDbContext(DbContextOptions<webSysdatecbackDbContext> options)
            : base(options)
        {
        }
        /*regiones*/
        public DbSet<PaisEntity> paisEntities { get; set; }
        public DbSet<DepartamentoEntity> departamentoEntities { get; set; }
        public DbSet<CiudadEntity> ciudadEntities { get; set; }

        public DbSet<CategoriaEntity> categoriaEntities { get; set; }
        public DbSet<CapacitacionesEntity> capacitacionesEntities { get; set; }
        public DbSet<DemosEntity> demosEntities { get; set; }
        public DbSet<MaterialVentaEntity> materialVentaEntities { get; set; }
        public DbSet<SolicitudesEntity> solicitudesEntities { get; set; }
        public DbSet<BlogEntity> blogEntities { get; set; }
        public DbSet<DescargasEntity> descargasEntities { get; set; }

        public DbSet<SolucionesEntity> solucionesEntities { get; set; }
        public DbSet<PlanesSolucioneEntity> planesSolucioneEntities { get; set; }

        public DbSet<TransaccionesPaypalEntity> transaccionesPaypalEntities { get; set; }
        public DbSet<PersonasEntity> personasEntities { get; set; }

        public DbSet<Conceptosentity> conceptosentities { get; set; }
        public DbSet<AsignacionesEntity> asignacionesEntities { get; set; }

        public DbSet<AlmacenamientoAdicionalEntity> almacenamientoAdicionalEntities { get; set; }
        public DbSet<CategoriaCotizadorEntity> categoriaCotizadorEntities { get; set; }
        public DbSet<LicenciaEntity> licenciaEntities { get; set; }
        public DbSet<ComentariosPostEntity> comentariosPostEntities { get; set; }
    }
}
