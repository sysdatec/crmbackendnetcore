using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace webSysdatecback.EntityFrameworkCore
{
    public static class webSysdatecbackDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<webSysdatecbackDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<webSysdatecbackDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection);
        }
    }
}
