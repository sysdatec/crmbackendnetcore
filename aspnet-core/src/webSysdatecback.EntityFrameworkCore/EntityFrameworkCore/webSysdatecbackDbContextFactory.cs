﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using webSysdatecback.Configuration;
using webSysdatecback.Web;

namespace webSysdatecback.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class webSysdatecbackDbContextFactory : IDesignTimeDbContextFactory<webSysdatecbackDbContext>
    {
        public webSysdatecbackDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<webSysdatecbackDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            webSysdatecbackDbContextConfigurer.Configure(builder, configuration.GetConnectionString(webSysdatecbackConsts.ConnectionStringName));

            return new webSysdatecbackDbContext(builder.Options);
        }
    }
}
