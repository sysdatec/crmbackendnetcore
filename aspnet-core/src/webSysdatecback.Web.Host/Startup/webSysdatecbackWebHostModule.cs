﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using webSysdatecback.Configuration;

namespace webSysdatecback.Web.Host.Startup
{
    [DependsOn(
       typeof(webSysdatecbackWebCoreModule))]
    public class webSysdatecbackWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public webSysdatecbackWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(webSysdatecbackWebHostModule).GetAssembly());
        }
    }
}
