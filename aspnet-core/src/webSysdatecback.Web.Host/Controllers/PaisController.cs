﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.Pais;
using webSysdatecback.Pais.PaisDto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class PaisController: webSysdatecbackControllerBase
    {
        [HttpPost("RegistraPais")]
        public async Task InsertUpdatePais([FromServices] IPaisAppService service, [FromForm] PaisInputDto inputDto)
        {
            await service.InsrtOrUpdatePais(inputDto);
        }

        [HttpGet("FindAll")]
        public async Task<List<PaisInputDto>> FindAll([FromServices] IPaisAppService service)
        {
            return await service.GetAllPais();
        }

        [HttpGet("FindByCodigo/{codigo}")]
        public async Task<PaisInputDto> FindByCodigo([FromServices] IPaisAppService service, string codigo)
        {
            return await service.GetByCodigo(codigo);
        }

    }
}
