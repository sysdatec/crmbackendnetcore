﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.Soluciones;
using webSysdatecback.Soluciones.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class SolucionesControllers : webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("CreaSolucion")]
        public async Task<bool> CreaSolucion([FromServices] ISolucionesAppService service, [FromForm] SolucionesDto inputDto)
        {
            return await service.RegistraActualizasolucion(inputDto);
        }
        [AbpAuthorize]
        [HttpGet("ConsultarSolucion/{nombre}")]
        public async Task<SolucionesOutDto> ConsultarSolucion([FromServices] ISolucionesAppService service, string nombre)
        {
            return await service.FindSolucionForName(nombre);
        }

        [AbpAuthorize]
        [HttpGet("ConsultarSoluciones")]
        public async Task<List<SolucionesOutDto>> ConsultarSoluciones([FromServices] ISolucionesAppService service)
        {
            return await service.FindSoluciones();
        }
        [AbpAuthorize]
        [HttpGet("ConsultarSolucionesporCategoria/{idCategoria}")]
        public async Task<List<SolucionesOutDto>> ConsultarSolucionesporCategoria([FromServices] ISolucionesAppService service, long idCategoria)
        {
            return await service.FindSolucionForCategoria(idCategoria);
        }

        [AbpAuthorize]
        [HttpDelete("EliminarSolucion/video")]
        public async Task<Boolean> EliminarSolucion([FromServices] ISolucionesAppService service, long solucion)
        {
            return await service.EliminaSolucion(solucion);
        }
    }
}
