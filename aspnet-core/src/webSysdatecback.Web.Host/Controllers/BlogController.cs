﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Blog;
using webSysdatecback.Blog.Dto;
using webSysdatecback.Controllers;
using webSysdatecback.Users;

namespace webSysdatecback.Web.Host.Controllers
{
    public class BlogController : webSysdatecbackControllerBase
    {
        [HttpPost("CrearModificaPost")]
        public async Task<bool> CrearModificaPost([FromServices] IBlogAppService service, [FromServices] IUserAppService serviceUser, [FromForm] BlogInputDto inputDto)
        {
            
            return await service.CrearoModificarPost(inputDto);
        }
        [HttpGet("ConsultarPost")]
        public async Task<List<BlogOutPtuDto>> ConsultarPost([FromServices] IBlogAppService service)
        {
            
            return await service.getAllBlog();
        }

        [HttpGet("ConsultarBlog")]
        public async Task<BlogOutPtuDto > ConsultarBlog([FromServices] IBlogAppService service)
        {

            return await service.UltimoRegistro();
        }

        [HttpDelete("EliminarPost/id")]
        public async Task<bool> EliminarPost([FromServices] IBlogAppService service, long id)
        {
            
            return await service.EliminarPost(id);
        }
    }
}
