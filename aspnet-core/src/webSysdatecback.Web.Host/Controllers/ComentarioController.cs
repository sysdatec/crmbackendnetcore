﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Comentarios;
using webSysdatecback.Comentarios.Dto;
using webSysdatecback.Controllers;

namespace webSysdatecback.Web.Host.Controllers
{
    public class ComentarioController: webSysdatecbackControllerBase
    {
        [HttpPost("CrearComentario")]
        public async Task<bool> CrearComentario([FromServices] IComentariosAppService service,  [FromForm] ComentarioInputDto inputDto)
        {

            return await service.RegistrarComentario(inputDto);
        }
        [HttpGet("ConsultarComentarios/{idBlog}")]
        public async Task<List<ComentarioOutPutDto>> ConsultarPost([FromServices] IComentariosAppService service, long idBlog)
        {

            return await service.ConsultarComentarios(idBlog);
        }
    }
}
