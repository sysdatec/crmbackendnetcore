﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Capacitaciones;
using webSysdatecback.Capacitaciones.Dto;
using webSysdatecback.Controllers;
using webSysdatecback.Descargas.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class CapacitacionesController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("RegistrarCapacitacion")]
        public async Task<bool> RegistrarCapacitacion([FromServices] ICapacitacionesAppService service, [FromForm] CapacitacionesInput inputDto)
        {
            return await service.AgregarCapacitaciones(inputDto);
        }

        [AbpAuthorize]
        [HttpPut("ActualizaCapacitacion")]
        public async Task<bool> ActualizaCapacitacion([FromServices] ICapacitacionesAppService service, [FromForm] ModificarCapacitacionesDto inputDto)
        {
            return await service.ActualizarCapacitaciones(inputDto);
        }

        [AbpAuthorize]
        [HttpGet("ConsultarCapacitaciones/{idCategoria}")]
        public async Task<List<CapacitacionesOutPut>> ConsultarCapacitaciones([FromServices] ICapacitacionesAppService service, string idCategoria)
        {
            return await service.getAllVideo(int.Parse(idCategoria));
        }

        [AbpAuthorize]
        [HttpGet("ConsultarVideosCapacitaciones")]
        public async Task<List<CapacitacionesOutPut>> ConsultarVideosCapacitaciones([FromServices] ICapacitacionesAppService service)
        {
            return await service.getAllVideos();
        }

        [AbpAuthorize]
        [HttpDelete("EliminarVideosCapacitaciones/video")]
        public async Task<Boolean> EliminarVideosCapacitaciones([FromServices] ICapacitacionesAppService service, long video)
        {
            return await service.EliminarCapacitaciones(video);
        }

        [AbpAuthorize]
        [HttpGet("ConsultarDescargas")]
        public async Task<List<DescargaOutPut>> ConsultarDescargas([FromServices] ICapacitacionesAppService service)
        {
            return await service.ConsultarDEscarga();
        }

        [AbpAuthorize]
        [HttpPost("RegistrarDescarga")]
        public async Task<bool> RegistrarDescarga([FromServices] ICapacitacionesAppService service, [FromForm] DescargaInputDto inputDto)
        {
            return await service.Agregardescarga(inputDto);
        }

        [AbpAuthorize]
        [HttpDelete("EliminarDescarga/descarga")]
        public async Task<Boolean> EliminarDescarga([FromServices] ICapacitacionesAppService service, long descarga)
        {
            return await service.EliminarDescarga(descarga);
        }


    }
}
