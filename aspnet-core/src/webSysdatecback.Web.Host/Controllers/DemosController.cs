﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.Demos;
using webSysdatecback.Demos.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class DemosController : webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("RegistrarDemos")]
        public async Task<bool> RegistrarDemos([FromServices] IDemosAppService service, [FromForm] DemosDtoInput inputDto)
        {
            return await service.AgregarDemo(inputDto);
        }

        [AbpAuthorize]
        [HttpPut("ActualizaDemos")]
        public async Task<bool> ActualizaDemos([FromServices] IDemosAppService service, [FromForm] ModificarDemosDto inputDto)
        {
            return await service.ActualizarDemo(inputDto);
        }

        [AbpAuthorize]
        [HttpGet("ConsultarDemos")]
        public async Task<List<DemosOutPut>> ConsultarCapacitaciones([FromServices] IDemosAppService service)
        {
            return await service.getAllDemos();
        }

        [AbpAuthorize]
        [HttpGet("ConsultarDemo/{idCategoria}")]
        public async Task<List<DemosOutPut>> ConsultarDemo([FromServices] IDemosAppService service, string idCategoria)
        {
            return await service.getAllDemo(int.Parse(idCategoria));
        }

        [AbpAuthorize]
        [HttpDelete("EliminarDemo/video")]
        public async Task<Boolean> EliminarVideosCapacitaciones([FromServices] IDemosAppService service, long video)
        {
            return await service.EliminarDemo(video);
        }
    }
}
