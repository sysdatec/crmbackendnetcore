﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.SendEmail;
using webSysdatecback.SendEmail.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class EnviarCorreocontroller : webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("SendEmail")]
        public async Task SendEmail([FromServices] ISenEmailAppService service, [FromForm] EnviarCorreoDto inputDto)
        {
             await service.SendEmail(inputDto);
        }
    }
}
