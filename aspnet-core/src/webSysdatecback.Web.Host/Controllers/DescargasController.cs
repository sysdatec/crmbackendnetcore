﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.Descargas;
using webSysdatecback.Descargas.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class DescargasController: webSysdatecbackControllerBase
    {
        
        [HttpGet("ConsultarDescargadd")]
        public async Task<List<DescargaOutPut>> ConsultarDescargadd([FromServices] DesacargaAppService service)
        {
            return await service.ConsultarDEscarga();
        }

        
        
        [HttpPost("InsertUpdateDescargadd")]
        public async Task<Boolean> InsertUpdateDescargadd([FromServices] DesacargaAppService service, [FromForm] DescargaInputDto input)
        {
            try
            {
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
            //return await service.RegistraDescarga(input);
            
        }

        [AbpAuthorize]
        [HttpDelete("EliminarDescargadd/{id}")]
        public async Task<Boolean> EliminarDescargadd([FromServices] DesacargaAppService service, int id)
        {
            return await service.EliminarDescarga(id);
        }
    }
}
