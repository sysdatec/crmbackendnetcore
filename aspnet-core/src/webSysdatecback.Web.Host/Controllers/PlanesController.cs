﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.Planes;
using webSysdatecback.Planes.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class PlanesController : webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("Registrarplan")]
        public async Task<bool> Registrarplan([FromServices] IPlanesAppService service, [FromForm] PlanesDto inputDto)
        {
            return await service.RegistraActualizaPlanes(inputDto);
        }
        [AbpAuthorize]
        [HttpGet("ConsultaPlan/{idPlan}")]
        public async Task<PlanesDto> ConsultaPlan([FromServices] IPlanesAppService service, long idPlan)
        {
            return await service.ConsultaPlan(idPlan);
        }

        [AbpAuthorize]
        [HttpGet("ConsultarPlanes")]
        public async Task<List<PlanesDto>> ConsultarPlanes([FromServices] IPlanesAppService service)
        {
            return await service.ConsultaPlanes();
        }

        [AbpAuthorize]
        [HttpDelete("EliminarPlan/plan")]
        public async Task<Boolean> EliminarPlan([FromServices] IPlanesAppService service, long plan)
        {
            return await service.EliminaPlan(plan);
        }
    }
}
