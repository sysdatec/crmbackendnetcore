﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Capacitaciones.Categoria;
using webSysdatecback.Capacitaciones.Dto;
using webSysdatecback.Controllers;

namespace webSysdatecback.Web.Host.Controllers
{
    public class CategoriasController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpGet("ConsultarCategoria/{modulo}")]
        public async Task<List<CategoriaInput>> ConsultarCategoria([FromServices] ICapacitacionesCategoryAppService service, string modulo)
        {
            return await service.ConsultarForModulo(modulo);
        }

        [AbpAuthorize]
        [HttpPost("InsertUpdateCategoria")]
        public async Task<Boolean> InsertUpdateCategoria([FromServices] ICapacitacionesCategoryAppService service, [FromForm] CategoriaInput input)
        {
            return await service.InsertOrUpdate(input);
        }

        [AbpAuthorize]
        [HttpDelete("ConsultarCategoria/{id}")]
        public async Task<Boolean> ConsultarCategoria([FromServices] ICapacitacionesCategoryAppService service, int id)
        {
            return await service.Delete(id);
        }
    }
}
