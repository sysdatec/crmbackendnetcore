﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.Soluciones;
using webSysdatecback.Transacciones.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class TransaccionesController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("RegistrarTransaccion")]
        public async Task<bool> RegistrarTransaccion([FromServices] ITransaccionesAppService service, [FromForm] TransaccionesDto inputDto)
        {
            return await service.RegistraTransaccion(inputDto);
        }
        
        [AbpAuthorize]
        [HttpGet("ConsultarTransacciones")]
        public async Task<List<TransaccionesDto>> ConsultarTransacciones([FromServices] ITransaccionesAppService service)
        {
            return await service.ConsultaTransacciones();
        }
    }
}
