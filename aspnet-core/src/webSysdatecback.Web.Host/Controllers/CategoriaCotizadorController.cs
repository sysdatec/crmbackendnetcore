﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.CategoriaCotizador;
using webSysdatecback.CategoriaCotizador.Dto;
using webSysdatecback.Controllers;

namespace webSysdatecback.Web.Host.Controllers
{
    public class CategoriaCotizadorController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("CreaCategoriaCotizador")]
        public async Task<bool> CreaCategoriaCotizador([FromServices] ICategoriaCotizadorAppService service, [FromForm] CategoriaCotizadorInputDto inputDto)
        {
            return await service.RegistrarCategoria(inputDto);
        }
        

        [AbpAuthorize]
        [HttpGet("ConsultarCategoriaCotizador")]
        public async Task<List<CategoriaCotizadorOutPutDto>> ConsultarCategoriaCotizador([FromServices] ICategoriaCotizadorAppService service)
        {
            return await service.ConsultarCategoria();
        }

        [AbpAuthorize]
        [HttpDelete("EliminarCategoriaCotizador/id")]
        public async Task<Boolean> EliminarCategoriaCotizador([FromServices] ICategoriaCotizadorAppService service, long id)
        {
            return await service.EliminarCategoria(id);
        }
    }
}
