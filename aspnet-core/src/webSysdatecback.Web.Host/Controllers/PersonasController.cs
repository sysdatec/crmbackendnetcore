﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.Personas;
using webSysdatecback.Personas.PersonasDto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class PersonasController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("RegistraPersona")]
        public async Task<bool> RegistraPersona([FromServices] IPersonasAppService service, [FromForm] PersonasInputDto inputDto)
        {
            return await service.RegistraActualizaPersona(inputDto);
        }
    
        

        [AbpAuthorize]
        [HttpGet("ConsultarPersonas")]
        public async Task<List<PersonasOutPutDto>> ConsultarPersonas([FromServices] IPersonasAppService service)
        {
            return await service.ConsultarPersonas();
        }

        [AbpAuthorize]
        [HttpDelete("EliminarVideosCapacitaciones/persona")]
        public async Task<Boolean> EliminarVideosCapacitaciones([FromServices] IPersonasAppService service, long persona)
        {
            return await service.EliminaPersona(persona);
        }
    }
}
