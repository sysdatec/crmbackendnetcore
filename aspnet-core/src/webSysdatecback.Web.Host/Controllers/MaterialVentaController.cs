﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.MaterialVenta;
using webSysdatecback.MaterialVenta.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class MaterialVentaController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("InsertUpdateMaterial")]
        public async Task<bool> InsertUpdateMaterial([FromServices] IMaterialVentaAppService service, [FromForm] MaterialInputDto inputDto)
        {
            return await service.AgregaOActualizaDocumento(inputDto);
        }
        [AbpAuthorize]
        [HttpGet("ConsultarMaterial/{idCategoria}")]
        public async Task<List<MaterialOutPutDto>> ConsultarMaterial([FromServices] IMaterialVentaAppService service, string idCategoria)
        {
            return await service.getAllDocumento(int.Parse(idCategoria));
        }

        [AbpAuthorize]
        [HttpGet("ConsultarMateriales")]
        public async Task<List<MaterialOutPutDto>> ConsultarMateriales([FromServices] IMaterialVentaAppService service)
        {
            return await service.getAllDocumentos();
        }

        [AbpAuthorize]
        [HttpDelete("EliminarMaterial/documento")]
        public async Task<Boolean> EliminarMaterial([FromServices] IMaterialVentaAppService service, long documento)
        {
            return await service.EliminarDocumento(documento);
        }
    }
}
