﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Asignaciones;
using webSysdatecback.Asignaciones.Dto;
using webSysdatecback.Controllers;

namespace webSysdatecback.Web.Host.Controllers
{
    public class AsignacionesController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("RegistrarAsignacion")]
        public async Task<bool> RegistrarAsignacion([FromServices] IAsignacionesAppService service, [FromForm] AsignacionesInputDto inputDto)
        {
            return await service.RegistraAsignaciones(inputDto);
        }
        [AbpAuthorize]
        [HttpGet("ConsultarAsignaciones")]
        public async Task<List<AsignacionesOutputDto>> ConsultarAsignaciones([FromServices] IAsignacionesAppService service)
        {
            return await service.ConsultaAsignaciones();
        }

        [AbpAuthorize]
        [HttpDelete("EliminarAsignacion/id")]
        public async Task<Boolean> EliminarAsignacion([FromServices] IAsignacionesAppService service, long id)
        {
            return await service.EliminaAsignaciones(id);
        }

        [AbpAuthorize]
        [HttpGet("ConsultarAsignaciones/{usuario}")]
        public async Task<List<AsignacionesOutputDto>> ConsultarAsignaciones([FromServices] IAsignacionesAppService service, string usuario)
        {
            return await service.ConsultaAsignacionesPorUsuario(usuario);
        }
    }
}
