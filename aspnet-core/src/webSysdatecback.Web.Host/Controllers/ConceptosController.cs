﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Conceptos;
using webSysdatecback.Conceptos.Dto;
using webSysdatecback.Controllers;

namespace webSysdatecback.Web.Host.Controllers
{
    public class ConceptosController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("RegistrarConcepto")]
        public async Task<bool> RegistrarConcepto([FromServices] IConceptosAppService service, [FromForm] ConceptosDto inputDto)
        {
            return await service.RegistraActualizaConceptos(inputDto);
        }
        [AbpAuthorize]
        [HttpGet("ConsultarConceptos")]
        public async Task<List<ConceptosDto>> ConsultarConceptos([FromServices] IConceptosAppService service)
        {
            return await service.ConsultarConcepto();
        }

        [AbpAuthorize]
        [HttpDelete("EliminarConcepto/id")]
        public async Task<Boolean> EliminarConcepto([FromServices] IConceptosAppService service, long id)
        {
            return await service.EliminarConceptos(id);
        }
    }
}
