﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Controllers;
using webSysdatecback.Licencia;
using webSysdatecback.Licencia.Dto;

namespace webSysdatecback.Web.Host.Controllers
{
    public class LicenciaController : webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpGet("ConsultarLicencia")]
        public async Task<List<LicenciaOutPutDto>> ConsultarLicencia([FromServices] ILicenciaAppService service)
        {
            return await service.ConsultaLicencia();
        }

        [AbpAuthorize]
        [HttpGet("ConsultarLicenciaPorSolucion/{idSolucion}")]
        public async Task<List<LicenciaOutPutDto>> ConsultarLicenciaPorSolucion([FromServices] ILicenciaAppService service, long idSolucion)
        {
            return await service.ConsultaLicenciaPorProducto(idSolucion);
        }

        [AbpAuthorize]
        [HttpPost("InsertUpdateLicencia")]
        public async Task<Boolean> InsertUpdateLicencia([FromServices] ILicenciaAppService service, [FromForm] LicenciaInputDto input)
        {
            return await service.RegistrarLicencia(input);
        }

        [AbpAuthorize]
        [HttpDelete("EliminarLicencia/{id}")]
        public async Task<Boolean> EliminarLicencia([FromServices] ILicenciaAppService service, int id)
        {
            return await service.EliminarLicencia(id);
        }
    }
}
