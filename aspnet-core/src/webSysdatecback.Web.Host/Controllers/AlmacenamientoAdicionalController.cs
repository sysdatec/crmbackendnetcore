﻿using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webSysdatecback.Almacenamiento;
using webSysdatecback.Almacenamiento.Dto;
using webSysdatecback.Controllers;

namespace webSysdatecback.Web.Host.Controllers
{
    public class AlmacenamientoAdicionalController: webSysdatecbackControllerBase
    {
        [AbpAuthorize]
        [HttpPost("RegistrarAlmacenamiento")]
        public async Task<bool> RegistrarAlmacenamiento([FromServices] IAlmacenamientoAppService service, [FromForm] AlmacenamientoInputDto inputDto)
        {
            return await service.RegistrarAlmacenamiento(inputDto);
        }
        [AbpAuthorize]
        [HttpGet("ConsultarAlmacenamiento")]
        public async Task<List<AlmacenamientoInputDto>> ConsultarAlmacenamiento([FromServices] IAlmacenamientoAppService service)
        {
            return await service.ConsultarAlmacenamiento();
        }

        [AbpAuthorize]
        [HttpDelete("EliminarAlmacenamiento/id")]
        public async Task<Boolean> EliminarAsignacion([FromServices] IAlmacenamientoAppService service, long id)
        {
            return await service.EliminaAlmacenamiento(id);
        }

        [AbpAuthorize]
        [HttpGet("ConsultarAlmacenamientoTipo/{tipo}")]
        public async Task<List<AlmacenamientoInputDto>> ConsultarAlmacenamientoTipo([FromServices] IAlmacenamientoAppService service, string tipo)
        {
            return await service.ConsultarAlmacenamientoPorTipo(tipo);
        }
    }
}
