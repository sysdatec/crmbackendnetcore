﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;
using webSysdatecback.MaterialVenta.Dto;

namespace webSysdatecback.AutoMapProfile
{
    public class MaterialVentaProfile : Profile
    {
        public MaterialVentaProfile()
        {
            CreateMap<MaterialVentaEntity, MaterialOutPutDto>();
            CreateMap<MaterialOutPutDto, MaterialVentaEntity>();
        }
    }
}
