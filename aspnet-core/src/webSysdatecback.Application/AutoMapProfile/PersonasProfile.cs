﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;
using webSysdatecback.Personas.PersonasDto;

namespace webSysdatecback.AutoMapProfile
{
    public class PersonasProfile:Profile
    {
        public PersonasProfile()
        {
            CreateMap<PersonasEntity, PersonasOutPutDto>();
            CreateMap<PersonasInputDto, PersonasEntity>();
        }
    }
}
