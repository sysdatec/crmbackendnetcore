﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Conceptos.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class ConceptosProfile: Profile
    {
        public ConceptosProfile()
        {
            CreateMap<Conceptosentity, ConceptosDto>();
            CreateMap<ConceptosDto, Conceptosentity>();
        }
    }
}
