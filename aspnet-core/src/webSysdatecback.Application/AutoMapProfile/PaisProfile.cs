﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;
using webSysdatecback.Pais.PaisDto;

namespace webSysdatecback.AutoMapProfile
{
    public class PaisProfile: Profile
    {
        public PaisProfile()
        {
            CreateMap<PaisEntity, PaisInputDto>();
            CreateMap<PaisInputDto, PaisEntity>();
        }

    }
}
