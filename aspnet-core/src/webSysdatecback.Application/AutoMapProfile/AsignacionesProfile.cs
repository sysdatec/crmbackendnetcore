﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Asignaciones.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class AsignacionesProfile:Profile
    {
        public AsignacionesProfile()
        {
            CreateMap<AsignacionesInputDto, AsignacionesEntity>();
            CreateMap<AsignacionesEntity, AsignacionesOutputDto>();
        }
    }
}
