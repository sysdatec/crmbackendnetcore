﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Blog.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class BlogProfile:Profile
    {
        public BlogProfile()
        {
            CreateMap<BlogEntity, BlogOutPtuDto>();
            
        }
    }
}
