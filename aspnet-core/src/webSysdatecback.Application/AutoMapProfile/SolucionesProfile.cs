﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;
using webSysdatecback.Soluciones.Dto;

namespace webSysdatecback.AutoMapProfile
{
    public class SolucionesProfile: Profile
    {
        public SolucionesProfile()
        {
            CreateMap<SolucionesEntity, SolucionesOutDto>();
            CreateMap<SolucionesDto, SolucionesEntity>();
        }
    }
}
