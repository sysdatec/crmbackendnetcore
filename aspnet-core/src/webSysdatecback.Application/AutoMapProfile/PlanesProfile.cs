﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;
using webSysdatecback.Planes.Dto;

namespace webSysdatecback.AutoMapProfile
{
    public class PlanesProfile: Profile
    {
        public PlanesProfile()
        {
            CreateMap<PlanesSolucioneEntity, PlanesDto>();
            CreateMap<PlanesDto, PlanesSolucioneEntity>();
        }
    }
}
