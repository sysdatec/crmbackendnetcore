﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Capacitaciones.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class CapacitacionesProfile: Profile
    {
        public CapacitacionesProfile()
        {
            CreateMap<CapacitacionesEntity,CapacitacionesOutPut>();
            CreateMap<CapacitacionesOutPut, CapacitacionesEntity>();
        }
    }
}
