﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Demos.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class DemosProfile: Profile
    {
        public DemosProfile()
        {
            CreateMap<DemosEntity, DemosOutPut>();
            CreateMap<DemosOutPut, DemosEntity>();
        }
    }
}
