﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;
using webSysdatecback.ProductoServicios.Dto;

namespace webSysdatecback.AutoMapProfile
{
    public class ProductoServicioProfile:Profile
    {
        public ProductoServicioProfile()
        {
            CreateMap<ProductoServiciosEntity, ProductoServiceDto>();
            CreateMap<ProductoServiceDto, ProductoServiciosEntity>();
        }
    }
}
