﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;
using webSysdatecback.Transacciones.Dto;

namespace webSysdatecback.AutoMapProfile
{
    public class TransaccionesProfile: Profile
    {
        public TransaccionesProfile()
        {
            CreateMap<TransaccionesPaypalEntity, TransaccionesDto>();
            CreateMap<TransaccionesDto, TransaccionesPaypalEntity>();
        }
    }
}
