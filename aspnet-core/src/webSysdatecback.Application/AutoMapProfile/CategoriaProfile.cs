﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Capacitaciones.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class CategoriaProfile: Profile
    {
        public CategoriaProfile()
        {
            CreateMap<CategoriaInput, CategoriaEntity>();
            CreateMap< CategoriaEntity, CategoriaInput>();
        }
    }
}
