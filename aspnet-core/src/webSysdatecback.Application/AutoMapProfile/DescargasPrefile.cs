﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Descargas.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class DescargasPrefile: Profile
    {
        public DescargasPrefile()
        {
            CreateMap<DescargaInputDto, DescargasEntity>();
            CreateMap<DescargasEntity, DescargaOutPut>();
        }
    }
}
