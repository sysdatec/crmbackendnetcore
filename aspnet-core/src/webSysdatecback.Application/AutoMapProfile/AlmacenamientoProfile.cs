﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Almacenamiento.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class AlmacenamientoProfile: Profile
    {
        public AlmacenamientoProfile()
        {
            CreateMap<AlmacenamientoAdicionalEntity, AlmacenamientoInputDto>();
            CreateMap<AlmacenamientoInputDto, AlmacenamientoAdicionalEntity>();
        }
    }
}
