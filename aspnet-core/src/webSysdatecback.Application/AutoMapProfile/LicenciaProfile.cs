﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;
using webSysdatecback.Licencia.Dto;

namespace webSysdatecback.AutoMapProfile
{
    public class LicenciaProfile: Profile
    {
        public LicenciaProfile()
        {
            CreateMap<LicenciaEntity, LicenciaOutPutDto>();
            CreateMap<LicenciaInputDto, LicenciaEntity>();
        }
    }
}
