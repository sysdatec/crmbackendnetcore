﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Comentarios.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class ComentarioProfile: Profile
    {
        public ComentarioProfile()
        {
            CreateMap<ComentariosPostEntity, ComentarioOutPutDto>();

        }
    }
}
