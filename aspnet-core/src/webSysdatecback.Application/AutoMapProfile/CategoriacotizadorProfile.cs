﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.CategoriaCotizador.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.AutoMapProfile
{
    public class CategoriacotizadorProfile:Profile
    {
        public CategoriacotizadorProfile()
        {
            CreateMap<CategoriaCotizadorInputDto,CategoriaCotizadorEntity>();
            CreateMap<CategoriaCotizadorEntity, CategoriaCotizadorOutPutDto>();
        }
    }
}
