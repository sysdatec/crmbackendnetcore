﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.Transacciones.Dto
{
    public class TransaccionesDto
    {
        public double precio { get; set; }
        public string detalle { get; set; }
        public PersonasEntity persona { get; set; }
        public long IdPersona { get; set; }
        public string estado { get; set; }
    }
}
