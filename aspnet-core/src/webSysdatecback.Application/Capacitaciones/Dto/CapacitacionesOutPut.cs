﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.Capacitaciones.Dto
{
    public class CapacitacionesOutPut
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public int IdCategoria { get; set; }
        public CategoriaInput categoria { get; set; }
        public string descripcion { get; set; }
        public string pathVideo { get; set; }
        public string url { get; set; }
    }
}
