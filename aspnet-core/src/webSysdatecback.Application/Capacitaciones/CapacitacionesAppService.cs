﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Capacitaciones.Dto;
using webSysdatecback.ClientHttp;
using webSysdatecback.Descargas;
using webSysdatecback.Descargas.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Capacitaciones
{
    public class CapacitacionesAppService : webSysdatecbackAppServiceBase, ICapacitacionesAppService
    {
        private readonly IRepository<DescargasEntity, long> _descargasRepository;
        private readonly IRepository<CapacitacionesEntity, long> _capacitacionesRepository;
        private readonly IRepository<CategoriaEntity> _categoriaRepository;
        private readonly IConfiguration _configuration = BuildConfiguration();

        public CapacitacionesAppService(IRepository<CapacitacionesEntity, long> capacitacionesRepository, IRepository<CategoriaEntity> categoriaRepository,
                                            IRepository<DescargasEntity, long> descargasRepository)
        {
            _capacitacionesRepository = capacitacionesRepository;
            _categoriaRepository = categoriaRepository;
            _descargasRepository = descargasRepository;
        }
        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Production.json", optional: true, reloadOnChange: true);
            return builder.Build();
        }

        public async Task<bool> ActualizarCapacitaciones(ModificarCapacitacionesDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = await _capacitacionesRepository.GetAsync(input.id);
                entity.titulo = input.titulo;
                entity.descripcion = input.descripcion;
                entity.IdCategoria = input.categoria;

                var resultado = await _capacitacionesRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> AgregarCapacitaciones(CapacitacionesInput input)
        {
            try
            {
                bool respuesta = false;
                string filePath = "";
                string nombre = "";
                if (input.video != null)
                {
                    string localFile = _configuration.GetValue<string>($"Archivos:localFile");
                    string pathVideo = _configuration.GetValue<string>($"Archivos:pathCapacitaciones");
                    string server = _configuration.GetValue<string>($"Archivos:serverFile");
                    if (localFile == "S")
                    {
                        var arrayName = input.video.FileName.Split(".");
                        var extension = arrayName[1];
                        nombre = _configuration.GetValue<string>($"service:videoCapacitaciones") + input.titulo + "." + extension;
                        
                        if (!Directory.Exists(pathVideo)) Directory.CreateDirectory(pathVideo);
                        filePath = pathVideo + input.titulo + "." + extension;

                        using (var stream = File.Create(filePath))
                        {
                            await input.video.CopyToAsync(stream);
                        }
                    }
                    else
                    {
                        UploadOutPut dato = new UploadOutPut();
                        dato.titulo = input.titulo;
                        dato.path = pathVideo;
                        dato.archivo = input.video;
                        EnviarArchivo enviar = new EnviarArchivo();
                        nombre = _configuration.GetValue<string>($"service:videoCapacitaciones") + await enviar.Post(server, dato);
                    }
                } else if (input.url!=null)
                {
                    nombre = input.url;
                }
                
                CapacitacionesEntity entity = new CapacitacionesEntity();
                entity.titulo = input.titulo;
                entity.pathVideo = filePath;
                entity.IdCategoria = input.categoria;
                entity.descripcion = input.descripcion;
                entity.url = nombre;
                var resultado = await _capacitacionesRepository.InsertAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;

                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> Agregardescarga(DescargaInputDto input)
        {
            try
            {
                bool respuesta = false;
                DescargasEntity entity = new DescargasEntity();
                if (input.id ==0)
                {
                    string filePath = "";
                    string nombre = "";
                    if (input.pathVideo != null)
                    {
                        string localFile = _configuration.GetValue<string>($"Archivos:localFile");
                        string pathVideo = _configuration.GetValue<string>($"Archivos:pathdescarga");
                        string server = _configuration.GetValue<string>($"Archivos:serverFile");
                        if (localFile == "S")
                        {
                            var arrayName = input.pathVideo.FileName.Split(".");
                            var extension = arrayName[1];
                            nombre = _configuration.GetValue<string>($"service:descarga") + input.titulo + "." + extension;
                            
                            if (!Directory.Exists(pathVideo)) Directory.CreateDirectory(pathVideo);
                            filePath = pathVideo + input.titulo + "." + extension;

                            using (var stream = File.Create(filePath))
                            {
                                await input.pathVideo.CopyToAsync(stream);
                            } 
                        }
                        else
                        {
                            UploadOutPut dato = new UploadOutPut();
                            dato.titulo = input.titulo;
                            dato.path = pathVideo;
                            dato.archivo = input.pathVideo;
                            EnviarArchivo enviar = new EnviarArchivo();
                            nombre = _configuration.GetValue<string>($"service:descarga") + await enviar.Post(server, dato);
                        }
                    }
                    else if (input.url != null)
                    {
                        nombre = input.url;
                    }

                    
                    entity.titulo = input.titulo;
                    entity.pathVideo = filePath;
                    entity.IdCategoria = input.IdCategoria;
                    entity.descripcion = input.descripcion;
                    entity.url = nombre;
                }
                else
                {
                    entity = ObjectMapper.Map<DescargasEntity>(input);
                }
                
                var id = await _descargasRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<DescargaOutPut>> ConsultarDEscarga()
        {
            try
            {
                var entity = await _descargasRepository.GetAll()
                            .Include(x => x.categoria).ToListAsync();
                return ObjectMapper.Map<List<DescargaOutPut>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
        public async Task<bool> EliminarDescarga(long id)
        {
            try
            {
                await _descargasRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminarCapacitaciones(long id)
        {
            try
            {
                _capacitacionesRepository.Delete(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<CapacitacionesOutPut>> getAllVideo(int idCategoria)
        {
            try
            {
                var consulta = await _capacitacionesRepository.GetAll()
                    .Where(x=>x.IdCategoria==idCategoria)
                    .Include(x => x.categoria)
                    .ToListAsync();
                
                return ObjectMapper.Map<List<CapacitacionesOutPut>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<CapacitacionesOutPut>> getAllVideos()
        {
            try
            {
                var consulta = await _capacitacionesRepository.GetAll()
                    .Include(x => x.categoria)
                    .Where(x=>x.categoria.modulo=="Capacitaciones")
                    .ToListAsync();

                return ObjectMapper.Map<List<CapacitacionesOutPut>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
