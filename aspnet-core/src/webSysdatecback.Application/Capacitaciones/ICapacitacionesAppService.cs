﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Capacitaciones.Dto;
using webSysdatecback.Descargas.Dto;

namespace webSysdatecback.Capacitaciones
{
    public interface ICapacitacionesAppService
    {
        Task<Boolean> AgregarCapacitaciones(CapacitacionesInput input);
        Task<Boolean> ActualizarCapacitaciones(ModificarCapacitacionesDto input);
        Task<List<CapacitacionesOutPut>> getAllVideo(int idCategoria);
        Task<List<CapacitacionesOutPut>> getAllVideos();
        Task<Boolean> EliminarCapacitaciones(long id);

        Task<Boolean> Agregardescarga(DescargaInputDto input);
        Task<Boolean> EliminarDescarga(long id);
        Task<List<DescargaOutPut>> ConsultarDEscarga();
    }
}
