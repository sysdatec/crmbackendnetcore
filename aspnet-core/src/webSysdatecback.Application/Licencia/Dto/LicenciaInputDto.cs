﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Licencia.Dto
{
    public class LicenciaInputDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public long IdSolucion { get; set; }
        public double valor { get; set; }
    }
}
