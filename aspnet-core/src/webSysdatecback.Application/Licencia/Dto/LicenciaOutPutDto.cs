﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.Licencia.Dto
{
    public class LicenciaOutPutDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public SolucionesEntity solucion { get; set; }
        public long IdSolucion { get; set; }
        public double valor { get; set; }
    }
}
