﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Licencia.Dto;

namespace webSysdatecback.Licencia
{
    public interface ILicenciaAppService
    {
        Task<bool> RegistrarLicencia(LicenciaInputDto input);
        Task<bool> EliminarLicencia(long id);
        Task<List<LicenciaOutPutDto>> ConsultaLicencia();
        Task<List<LicenciaOutPutDto>> ConsultaLicenciaPorProducto(long id);
    }
}
