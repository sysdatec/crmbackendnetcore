﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Entidades;
using webSysdatecback.Licencia.Dto;

namespace webSysdatecback.Licencia
{
    public class LicenciaAppService : webSysdatecbackAppServiceBase, ILicenciaAppService
    {
        private readonly IRepository<LicenciaEntity, long> _licenciaRepository;
        public LicenciaAppService(IRepository<LicenciaEntity, long> licenciaRepository)
        {
            _licenciaRepository = licenciaRepository;
        }
        public async Task<List<LicenciaOutPutDto>> ConsultaLicencia()
        {
            try
            {
                var entity = await _licenciaRepository.GetAll()
                            .Include(x => x.solucion)
                            .Include(x=>x.solucion.categoria)
                            .ToListAsync();
                return ObjectMapper.Map<List<LicenciaOutPutDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<LicenciaOutPutDto>> ConsultaLicenciaPorProducto(long id)
        {
            try
            {
                var entity = await _licenciaRepository.GetAll()
                            .Include(x => x.solucion)
                            .Include(x => x.solucion.categoria)
                            .Where(x=>x.IdSolucion==id)
                            .ToListAsync();
                return ObjectMapper.Map<List<LicenciaOutPutDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminarLicencia(long id)
        {
            try
            {
                await _licenciaRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistrarLicencia(LicenciaInputDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<LicenciaEntity>(input);
                var id = await _licenciaRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
