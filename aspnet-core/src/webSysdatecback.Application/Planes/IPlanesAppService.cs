﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Planes.Dto;

namespace webSysdatecback.Planes
{
    public interface IPlanesAppService
    {
        Task<Boolean> RegistraActualizaPlanes(PlanesDto input);
        Task<PlanesDto> ConsultaPlan(long id);
        Task<List<PlanesDto>> ConsultaPlanes();
        Task<Boolean> EliminaPlan(long id);
    }
}
