﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.Planes.Dto
{
    public class PlanesDto
    {
        public long id { get; set; }
        public int limiteUsuarios { get; set; }
        public SolucionesEntity soluciones { get; set; }
        public long IdSolucion { get; set; }
        public double valor { get; set; }
        public string nivel { get; set; }
    }
}
