﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Entidades;
using webSysdatecback.Planes.Dto;

namespace webSysdatecback.Planes
{
    public class PlanesAppService : webSysdatecbackAppServiceBase, IPlanesAppService
    {
        private readonly IRepository<PlanesSolucioneEntity, long> _planesRepository;
        //private readonly IRepository<SolicitudesEntity> _solucionRepository;
        public PlanesAppService(IRepository<PlanesSolucioneEntity, long> planesRepository)
        {
            _planesRepository = planesRepository;
        }
        public async Task<PlanesDto> ConsultaPlan(long id)
        {
            try
            {
                var consulta = await _planesRepository.GetAll()
                    .Where(x => x.Id == id)
                    .Include(x => x.soluciones)
                    .FirstOrDefaultAsync();

                return ObjectMapper.Map< PlanesDto> (consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<PlanesDto>> ConsultaPlanes()
        {
            try
            {
                var consulta = await _planesRepository.GetAll()
                    .Include(x => x.soluciones)
                    .ToListAsync();

                return ObjectMapper.Map<List<PlanesDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminaPlan(long id)
        {
            try
            {
                await _planesRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraActualizaPlanes(PlanesDto input)
        {
            try
            {
                Boolean respuesta = false;
                var entity = ObjectMapper.Map<PlanesSolucioneEntity>(input);
                var id = await _planesRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                    return respuesta;
    }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
