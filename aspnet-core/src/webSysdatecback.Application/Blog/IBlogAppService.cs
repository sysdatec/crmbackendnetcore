﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Blog.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Blog
{
    public interface IBlogAppService
    {
        Task<Boolean> CrearoModificarPost(BlogInputDto input);
        Task<Boolean> EliminarPost(long id);
        Task<List<BlogOutPtuDto>> getAllBlog();
        Task<BlogOutPtuDto> getBlog(long id);
        Task<BlogOutPtuDto> UltimoRegistro();
    }
}
