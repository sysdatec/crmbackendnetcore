﻿using Abp.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Authorization.Users;

namespace webSysdatecback.Blog.Dto
{
    public class BlogOutPtuDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public string contenido { get; set; }
        public string urlContenido { get; set; }
        public DateTime CreationTime { get; set; }
        public string autor { get; set; }
        public long CreatorUserId { get; set; }
        public string tipo { get; set; }
    }
}
