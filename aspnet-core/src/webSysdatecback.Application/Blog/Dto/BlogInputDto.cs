﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Blog.Dto
{
    public class BlogInputDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public string contenido { get; set; }
        public IFormFile file { get; set; }
        public string tipoContenido { get; set; }
        public string url { get; set; }
        public long idUser { get; set; }
        public string autor { get; set; }
    }
}
