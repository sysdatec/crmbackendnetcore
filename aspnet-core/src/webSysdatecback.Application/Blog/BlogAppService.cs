﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Authorization.Users;
using webSysdatecback.Blog.Dto;
using webSysdatecback.ClientHttp;
using webSysdatecback.Entidades;

namespace webSysdatecback.Blog
{
    public class BlogAppService : webSysdatecbackAppServiceBase, IBlogAppService
    {
        private readonly IRepository<BlogEntity, long> _bloggerRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IConfiguration _configuration = BuildConfiguration();

        public BlogAppService(IRepository<BlogEntity, long> bloggerRepository, IRepository<User, long> userRepository)
        {
            _bloggerRepository = bloggerRepository;
            _userRepository = userRepository;


        }
        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Production.json", optional: true, reloadOnChange: true);
            return builder.Build();
        }

        public async Task<bool> CrearoModificarPost(BlogInputDto input)
        {
            try
            {
                bool respuesta = false;
                string filePath = "";
                string nombre = "";
                var tipoArchivo = input.tipoContenido;
                string pathVideo = "";
                string servicio = "";
                var autor = await _userRepository.GetAll().Where(x=>x.Id==input.idUser).FirstOrDefaultAsync();
                if (input.id == 0)
                {
                   
                    pathVideo =  _configuration.GetValue<string>($"Archivos:pathblog");
                    string localFile = _configuration.GetValue<string>($"Archivos:localFile");
                    string server = _configuration.GetValue<string>($"Archivos:serverFile");
                    if (localFile == "S")
                    {
                        if (input.file != null)
                        {
                            var arrayName = input.file.FileName.Split(".");
                            var extension = arrayName[1];
                            servicio = _configuration.GetValue<string>($"service:blog") + input.titulo.Trim() + "." + extension;
                            nombre = Path.Combine(pathVideo, input.titulo + "." + extension);
                            await uploadFile(input.file, pathVideo, nombre);
                        }
                        else
                        {
                            servicio = input.url;
                        }
                        
                    }
                    else
                    {
                        if (input.file != null)
                        {
                            UploadOutPut dato = new UploadOutPut();
                            dato.titulo = input.titulo;
                            dato.path = pathVideo;
                            dato.archivo = input.file;
                            EnviarArchivo enviar = new EnviarArchivo();
                            nombre = await enviar.Post(server, dato);
                            servicio = _configuration.GetValue<string>($"service:blog") + nombre;
                        }
                        else
                        {
                            servicio = input.url;
                        }
                    }
                }
                else
                {
                    servicio = input.url;
                }
               

                BlogEntity entity = new BlogEntity();
                entity.contenido = input.contenido;
                entity.titulo = input.titulo;
                entity.Id = input.id;
                entity.urlContenido = servicio;
                entity.autor = autor.Name + ' ' + autor.Surname;
                entity.tipo = input.tipoContenido;

                var id = await _bloggerRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;

                return respuesta;
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }
        }

        private async Task uploadFile(IFormFile archivo, string ruta, string nombre)
        {
            try
            {
                 
                    
                    if (!Directory.Exists(ruta)) Directory.CreateDirectory(ruta);
                   
                    using (var stream = File.Create(nombre))
                    {
                        await archivo.CopyToAsync(stream);
                    }
               
            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<bool> EliminarPost(long id)
        {
            try
            {
                await _bloggerRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<BlogOutPtuDto>> getAllBlog()
        {
            try
            {
                var entity = await _bloggerRepository.GetAllListAsync();
                
                return ObjectMapper.Map<List<BlogOutPtuDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<BlogOutPtuDto> getBlog(long id)
        {
            try
            {
                var entity = await _bloggerRepository.GetAll().FirstOrDefaultAsync();
                return ObjectMapper.Map<BlogOutPtuDto>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<BlogOutPtuDto> UltimoRegistro()
        {
            try
            {
                var entity = await _bloggerRepository.GetAll()
                                .OrderByDescending(x => x.Id)
                                .FirstOrDefaultAsync();
                return ObjectMapper.Map<BlogOutPtuDto>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
