﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Entidades;
using webSysdatecback.Soluciones.Dto;

namespace webSysdatecback.Soluciones
{
    public class SolucionesAppService : webSysdatecbackAppServiceBase, ISolucionesAppService
    {
        private readonly IRepository<SolucionesEntity, long> _solucionesRepository;
        public SolucionesAppService(IRepository<SolucionesEntity, long> solucionesRepository)
        {
            _solucionesRepository = solucionesRepository;
        }
        public async Task<bool> EliminaSolucion(long id)
        {
            try
            {
               await  _solucionesRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<SolucionesOutDto>> FindSoluciones()
        {
            try
            {
                var Consulta = await _solucionesRepository.GetAll().Include(x=>x.categoria).ToListAsync();
                return ObjectMapper.Map<List<SolucionesOutDto>>(Consulta);

    }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);

            }
        }

        public async Task<List<SolucionesOutDto>> FindSolucionForCategoria(long id)
        {
            try
            {
                var Consulta = await _solucionesRepository.GetAll().Include(x => x.categoria)
                    .Where(x=>x.IdCategoria==id).ToListAsync();
                return ObjectMapper.Map<List<SolucionesOutDto>>(Consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<SolucionesOutDto> FindSolucionForName(string nombre)
        {
            try
            {
                var Consulta = await _solucionesRepository.GetAll().Where(x=>x.nombre==nombre).FirstOrDefaultAsync();
                return ObjectMapper.Map<SolucionesOutDto>(Consulta);

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);

            }
        }

        public async Task<bool> RegistraActualizasolucion(SolucionesDto input)
        {
            try
            {
                Boolean respuesta = false;
                var entity = ObjectMapper.Map<SolucionesEntity>(input);
                var id = await _solucionesRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
