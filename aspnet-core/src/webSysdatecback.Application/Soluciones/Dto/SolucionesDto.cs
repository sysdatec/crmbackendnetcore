﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Soluciones.Dto
{
    public class SolucionesDto
    {
        public long id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public long IdCategoria { get; set; }
    }
}
