﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.Soluciones.Dto
{
    public class SolucionesOutDto
    {
        public long id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public CategoriaCotizadorEntity categoria { get; set; }
        public long IdCategoria { get; set; }
    }
}
