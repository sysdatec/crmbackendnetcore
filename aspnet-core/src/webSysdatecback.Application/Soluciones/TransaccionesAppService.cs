﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Entidades;
using webSysdatecback.Transacciones.Dto;

namespace webSysdatecback.Soluciones
{
    public class TransaccionesAppService : webSysdatecbackAppServiceBase, ITransaccionesAppService
    {
        private readonly IRepository<TransaccionesPaypalEntity, long> _transaccionesRepository;
        public TransaccionesAppService(IRepository<TransaccionesPaypalEntity, long> transaccionesRepository)
        {
            _transaccionesRepository = transaccionesRepository;
        }
        public async Task<List<TransaccionesDto>> ConsultaTransacciones()
        {
            try
            {
                var consulta = await _transaccionesRepository.GetAll()
                    .Include(x => x.persona)
                    .ToListAsync();

                return ObjectMapper.Map<List<TransaccionesDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraTransaccion(TransaccionesDto input)
        {
            try
            {
                Boolean respuesta = false;
                var entity = ObjectMapper.Map<TransaccionesPaypalEntity>(input);
                var id = await _transaccionesRepository.InsertAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
