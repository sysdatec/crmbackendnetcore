﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Soluciones.Dto;

namespace webSysdatecback.Soluciones
{
    public interface ISolucionesAppService
    {
        Task<Boolean> RegistraActualizasolucion(SolucionesDto input);
        Task<Boolean> EliminaSolucion(long id);
        Task<List<SolucionesOutDto>> FindSoluciones();
        Task<SolucionesOutDto> FindSolucionForName(string nombre);
        Task<List<SolucionesOutDto>> FindSolucionForCategoria(long id);
    }
}
