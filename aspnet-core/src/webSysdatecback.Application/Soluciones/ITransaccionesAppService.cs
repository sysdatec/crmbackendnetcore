﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Transacciones.Dto;

namespace webSysdatecback.Soluciones
{
    public interface ITransaccionesAppService
    {
        Task<Boolean> RegistraTransaccion(TransaccionesDto input);
        Task<List<TransaccionesDto>> ConsultaTransacciones();
    }
}
