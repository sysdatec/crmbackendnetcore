﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Demos.Dto;

namespace webSysdatecback.Demos
{
    public interface IDemosAppService
    {
        Task<Boolean> AgregarDemo(DemosDtoInput input);
        Task<Boolean> ActualizarDemo(ModificarDemosDto input);
        Task<List<DemosOutPut>> getAllDemos();
        Task<List<DemosOutPut>> getAllDemo(int idCategoria);
        Task<Boolean> EliminarDemo(long id);
    }
}
