﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Demos.Dto
{
    public class DemosDtoInput
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public int categoria { get; set; }
        public string descripcion { get; set; }
        public IFormFile video { get; set; }
        public string url { get; set; }
    }
}
