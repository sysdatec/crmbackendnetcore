﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Demos.Dto
{
    public class ModificarDemosDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public int categoria { get; set; }
        public string descripcion { get; set; }
    }
}
