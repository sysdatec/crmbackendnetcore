﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.ClientHttp;
using webSysdatecback.Demos.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Demos
{
    public class DemosAppService : webSysdatecbackAppServiceBase, IDemosAppService
    {
        private readonly IRepository<DemosEntity, long> _demosRepository;
        private readonly IConfiguration _configuration = BuildConfiguration();

        public DemosAppService(IRepository<DemosEntity, long> demosRepository)
        {
            _demosRepository = demosRepository;
        }
        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Production.json", optional: true, reloadOnChange: true);
            return builder.Build();
        }
        public async Task<bool> ActualizarDemo(ModificarDemosDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = await _demosRepository.GetAsync(input.id);
                entity.titulo = input.titulo;
                entity.descripcion = input.descripcion;
                entity.IdCategoria = input.categoria;

                var resultado = await _demosRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> AgregarDemo(DemosDtoInput input)
        {
            try
            {
                bool respuesta = false;
                string filePath = "";
                string nombre = "";
                if (input.video != null)
                {
                    string localFile = _configuration.GetValue<string>($"Archivos:localFile");
                    string pathVideo = _configuration.GetValue<string>($"Archivos:pathdemo");
                    string server = _configuration.GetValue<string>($"Archivos:serverFile");
                    if (localFile == "S")
                    {
                        var arrayName = input.video.FileName.Split(".");
                        var extension = arrayName[1];
                        nombre = _configuration.GetValue<string>($"service:videoDemos") + input.titulo + "." + extension;
                       
                        if (!Directory.Exists(pathVideo)) Directory.CreateDirectory(pathVideo);
                        filePath = pathVideo + input.titulo + "." + extension;

                        using (var stream = File.Create(filePath))
                        {
                            await input.video.CopyToAsync(stream);
                        } 
                    }
                    else
                    {
                        UploadOutPut dato = new UploadOutPut();
                        dato.titulo = input.titulo;
                        dato.path = pathVideo;
                        dato.archivo = input.video;
                        EnviarArchivo enviar = new EnviarArchivo();
                        nombre = _configuration.GetValue<string>($"service:videoDemos") + await enviar.Post(server, dato);
                    }
                }
                else if (input.url !=null)
                {
                    nombre = input.url;
                }

                DemosEntity entity = new DemosEntity();
                entity.titulo = input.titulo;
                entity.pathVideo = filePath;
                entity.IdCategoria = input.categoria;
                entity.descripcion = input.descripcion;
                entity.url = nombre;
                var resultado = await _demosRepository.InsertAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;

                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminarDemo(long id)
        {
            try
            {
                _demosRepository.Delete(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<DemosOutPut>> getAllDemo(int idCategoria)
        {
            try
            {
                var consulta = await _demosRepository.GetAll()
                    .Where(x => x.IdCategoria == idCategoria)
                    .Include(x => x.categoria)
                    .ToListAsync();

                return ObjectMapper.Map<List<DemosOutPut>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<DemosOutPut>> getAllDemos()
        {
            try
            {
                var consulta = await _demosRepository.GetAll()
                    .Include(x => x.categoria)
                    .Where(x => x.categoria.modulo == "Demos")
                    .ToListAsync();

                return ObjectMapper.Map<List<DemosOutPut>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
