﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Pais.PaisDto
{
    public class PaisInputDto
    {
        public long id { get; set; }
        public string nombrePais { get; set; }
        public string codigo { get; set; }
    }
}
