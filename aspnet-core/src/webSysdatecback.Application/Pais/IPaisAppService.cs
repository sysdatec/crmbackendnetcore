﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Pais.PaisDto;

namespace webSysdatecback.Pais
{
    public interface IPaisAppService
    {
        Task<Boolean> InsrtOrUpdatePais(PaisInputDto inputDto);
        Task<List<PaisInputDto>> GetAllPais();
        Task<PaisInputDto> GetByCodigo(string codigo);
    }
}
