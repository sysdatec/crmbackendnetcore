﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Entidades;
using webSysdatecback.Pais.PaisDto;

namespace webSysdatecback.Pais
{
    public class PaisAppService : webSysdatecbackAppServiceBase, IPaisAppService
    {
        private readonly IRepository<PaisEntity,long> _paisRepository;

        public PaisAppService(IRepository<PaisEntity,long> paisRepository)
        {
            _paisRepository = paisRepository;
        }

        public async Task<List<PaisInputDto>> GetAllPais()
        {
            try
            {
                var resultado = await _paisRepository.GetAllListAsync();
                return ObjectMapper.Map<List<PaisInputDto>>(resultado);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PaisInputDto> GetByCodigo(string codigo)
        {
            try
            {
                var resultado = await _paisRepository.GetAll().Where(x=>x.codigo==codigo).FirstOrDefaultAsync();
                return ObjectMapper.Map<PaisInputDto>(resultado);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> InsrtOrUpdatePais(PaisInputDto inputDto)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<PaisEntity>(inputDto);
                var resultado = await _paisRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
