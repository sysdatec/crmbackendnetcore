﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.ClientHttp;
using webSysdatecback.Entidades;
using webSysdatecback.MaterialVenta.Dto;

namespace webSysdatecback.MaterialVenta
{
    public class MaterialVentaAppService : webSysdatecbackAppServiceBase, IMaterialVentaAppService
    {
        private readonly IRepository<MaterialVentaEntity, long> _materialVentaRepository;
        private readonly IRepository<CategoriaEntity> _categoriaRepository;
        private readonly IConfiguration _configuration = BuildConfiguration();

        public MaterialVentaAppService(IRepository<MaterialVentaEntity, long> materialVentaRepository, IRepository<CategoriaEntity> categoriaRepository)
        {
            _materialVentaRepository = materialVentaRepository;
            _categoriaRepository = categoriaRepository;
        }
        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Production.json", optional: true, reloadOnChange: true);
            return builder.Build();
        }
        public async Task<bool> AgregaOActualizaDocumento(MaterialInputDto input)
        {
            try
            {
                MaterialVentaEntity entity = new MaterialVentaEntity();
                bool respuesta = false;
                string filePath = "";
                string nombre = "";
                string localFile = _configuration.GetValue<string>($"Archivos:localFile");
                if (input.id > 0)
                {
                     entity = await _materialVentaRepository.GetAll().Where(x=>x.Id==input.id).FirstOrDefaultAsync();
                    entity.titulo = input.titulo;
                    entity.descripcion = input.descripcion;
                }
                else
                {
                     entity = new MaterialVentaEntity();
                    if (input.documento != null)
                    {
                        string pathVideo = _configuration.GetValue<string>($"Archivos:pathmaterial");
                        string server = _configuration.GetValue<string>($"Archivos:serverFile");


                        if (localFile == "S")
                        {
                            var arrayName = input.documento.FileName.Split(".");
                            var extension = arrayName[1];
                            nombre = _configuration.GetValue<string>($"service:materialVenta") + input.titulo + "." + extension;
                            
                            if (!Directory.Exists(pathVideo)) Directory.CreateDirectory(pathVideo);
                            filePath = pathVideo + input.titulo + "." + extension;

                            using (var stream = File.Create(filePath))
                            {
                                await input.documento.CopyToAsync(stream);
                            }
                        }
                        else {
                            UploadOutPut  dato= new UploadOutPut();
                            dato.titulo = input.titulo;
                            dato.path = pathVideo;
                            dato.archivo = input.documento;
                            EnviarArchivo enviar = new EnviarArchivo();
                            nombre = _configuration.GetValue<string>($"service:materialVenta") + await enviar.Post(server, dato);
                        }
                    }
                    else if (input.url != null)
                    {
                        nombre = input.url;
                    }
                    entity.titulo = input.titulo;
                    entity.pathDocumento = filePath;
                    entity.IdCategoria = input.categoria;
                    entity.descripcion = input.descripcion;
                    entity.url = nombre;
                }
                           
                var resultado = await _materialVentaRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;

                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminarDocumento(long id)
        {
            try
            {
                _materialVentaRepository.Delete(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<MaterialOutPutDto>> getAllDocumento(int idCategoria)
        {
            try
            {
                var consulta = await _materialVentaRepository.GetAll()
                    .Where(x => x.IdCategoria == idCategoria)
                    .Include(x => x.categoria)
                    .ToListAsync();

                return ObjectMapper.Map<List<MaterialOutPutDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<MaterialOutPutDto>> getAllDocumentos()
        {
            try
            {
                var consulta = await _materialVentaRepository.GetAll()
                    .Include(x => x.categoria)
                    .Where(x => x.categoria.modulo == "MaterialVenta")
                    .ToListAsync();

                return ObjectMapper.Map<List<MaterialOutPutDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
