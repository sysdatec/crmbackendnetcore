﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.MaterialVenta.Dto;

namespace webSysdatecback.MaterialVenta
{
    public interface IMaterialVentaAppService
    {
        Task<Boolean> AgregaOActualizaDocumento(MaterialInputDto input);
        Task<List<MaterialOutPutDto>> getAllDocumento(int idCategoria);
        Task<List<MaterialOutPutDto>> getAllDocumentos();
        Task<Boolean> EliminarDocumento(long id);
    }
}
