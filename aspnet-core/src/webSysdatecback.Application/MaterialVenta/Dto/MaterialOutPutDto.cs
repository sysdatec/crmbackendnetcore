﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Capacitaciones.Dto;

namespace webSysdatecback.MaterialVenta.Dto
{
    public class MaterialOutPutDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public int IdCategoria { get; set; }
        public CategoriaInput categoria { get; set; }
        public string descripcion { get; set; }
        public string pathpathDocumento { get; set; }
        public string url { get; set; }
    }
}
