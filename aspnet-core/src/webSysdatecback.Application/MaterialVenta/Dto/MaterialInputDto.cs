﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.MaterialVenta.Dto
{
    public class MaterialInputDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public int categoria { get; set; }
        public string descripcion { get; set; }
        public IFormFile documento { get; set; }
        public string url { get; set; }
    }
}
