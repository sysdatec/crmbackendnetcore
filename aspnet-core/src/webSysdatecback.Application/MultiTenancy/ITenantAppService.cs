﻿using Abp.Application.Services;
using webSysdatecback.MultiTenancy.Dto;

namespace webSysdatecback.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

