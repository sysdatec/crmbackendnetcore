﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Capacitaciones.Dto
{
    public class CategoriaInput
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string modulo { get; set; }
    }
}
