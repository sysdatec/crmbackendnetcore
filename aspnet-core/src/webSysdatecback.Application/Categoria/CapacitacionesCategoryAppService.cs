﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Capacitaciones.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Capacitaciones.Categoria
{
    public class CapacitacionesCategoryAppService : webSysdatecbackAppServiceBase, ICapacitacionesCategoryAppService
    {
        private readonly IRepository<CategoriaEntity> _categoriaRepository;
        public CapacitacionesCategoryAppService(IRepository<CategoriaEntity> categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }
        public async Task<List<CategoriaInput>> ConsultarCategorias()
        {
            try
            {
                var entity = await _categoriaRepository.GetAllListAsync();
                return ObjectMapper.Map<List<CategoriaInput>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<CategoriaInput> ConsultarForId(int id)
        {
            try
            {
                var entity = await _categoriaRepository.GetAll().Where(x=>x.Id==id).FirstOrDefaultAsync();
                return ObjectMapper.Map<CategoriaInput>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<CategoriaInput>> ConsultarForModulo(string modulo)
        {
            try
            {
                var consulta = await _categoriaRepository.GetAll().Where(x => x.modulo == modulo).ToListAsync();
                return ObjectMapper.Map<List<CategoriaInput>>(consulta);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                await _categoriaRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> InsertOrUpdate(CategoriaInput input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<CategoriaEntity>(input);
                var resultado = await _categoriaRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
