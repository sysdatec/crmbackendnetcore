﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Capacitaciones.Dto;

namespace webSysdatecback.Capacitaciones.Categoria
{
    public interface ICapacitacionesCategoryAppService
    {
        Task<Boolean> InsertOrUpdate(CategoriaInput input);
        Task<Boolean> Delete(int id);
        Task<List<CategoriaInput>> ConsultarCategorias();
        Task<CategoriaInput> ConsultarForId(int id);
        Task<List<CategoriaInput>> ConsultarForModulo(string modulo);
    }
}
