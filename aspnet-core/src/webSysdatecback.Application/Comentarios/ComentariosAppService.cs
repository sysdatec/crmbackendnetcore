﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Authorization.Users;
using webSysdatecback.Comentarios.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Comentarios
{
    public class ComentariosAppService : webSysdatecbackAppServiceBase, IComentariosAppService
    {
        private readonly IRepository<ComentariosPostEntity, long> _comentariosRepository;
        private readonly IRepository<User, long> _userRepository;
        public ComentariosAppService(IRepository<ComentariosPostEntity, long> comentariosRepository, IRepository<User, long> userRepository)
        {
            _comentariosRepository = comentariosRepository;
            _userRepository = userRepository;


        }
        public async Task<List<ComentarioOutPutDto>> ConsultarComentarios(long idBlog)
        {
            try
            {
                var entity = await _comentariosRepository.GetAll().Where(x=>x.IdBlog==idBlog).ToListAsync();
                return ObjectMapper.Map<List<ComentarioOutPutDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistrarComentario(ComentarioInputDto input)
        {
            try
            {
                bool respuesta= false;
                var usuario = await _userRepository.GetAll().Where(x => x.Id == input.idUser).FirstOrDefaultAsync();
                ComentariosPostEntity entity = new ComentariosPostEntity();
                entity.IdBlog = input.IdBlog;
                entity.Id = input.id;
                entity.usuario = usuario.FullName;
                entity.cometario = input.cometario;
                var id = await _comentariosRepository.InsertAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
