﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.Comentarios.Dto
{
    public class ComentarioOutPutDto
    {
        public long id { get; set; }
        public string cometario { get; set; }
        public long IdBlog { get; set; }
        public string usuario { get; set; }
    }
}
