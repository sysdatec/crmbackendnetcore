﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Comentarios.Dto
{
    public class ComentarioInputDto
    {
        public long id { get; set; }
        public string cometario { get; set; }
        public long IdBlog { get; set; }
        public long idUser { get; set; }
    }
}
