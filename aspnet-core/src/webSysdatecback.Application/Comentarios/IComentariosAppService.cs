﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Comentarios.Dto;

namespace webSysdatecback.Comentarios
{
    public interface IComentariosAppService
    {
        Task<Boolean> RegistrarComentario(ComentarioInputDto input);
        Task<List<ComentarioOutPutDto>> ConsultarComentarios(long idBlog);
    }
}
