﻿using Abp.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace webSysdatecback.ClientHttp
{
    public class EnviarArchivo
    {
        public async Task<string> Post(string controlador, UploadOutPut json)
        {
            try
            {
                string respuesta = "";
                HttpClient client;
                client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                var url = controlador;
                byte[] data;
                using (var br = new BinaryReader(json.archivo.OpenReadStream()))
                    data = br.ReadBytes((int)json.archivo.OpenReadStream().Length);

                ByteArrayContent bytes = new ByteArrayContent(data);
                MultipartFormDataContent form = new MultipartFormDataContent();

                form.Add(new StringContent(json.titulo), "titulo" );
                form.Add(new StringContent(json.path), "path");
                form.Add(bytes, "archivo", json.archivo.FileName);


                HttpResponseMessage response = null;
                response = await client.PostAsync(url, form);

                if (response.IsSuccessStatusCode)
                {
                    respuesta = await response.Content.ReadAsStringAsync();
                   
                }

                return respuesta;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return msg;
            }
        }

    }
}
