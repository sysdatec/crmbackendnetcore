﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.ClientHttp
{
    public class UploadOutPut
    {
        public string titulo { get; set; }
        public string path { get; set; }
        public IFormFile archivo { get; set; }
    }
}
