﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Asignaciones.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Asignaciones
{
    public class AsignacionesAppService : webSysdatecbackAppServiceBase, IAsignacionesAppService
    {
        private readonly IRepository<AsignacionesEntity, long> _asignacionesRepository;

        public AsignacionesAppService(IRepository<AsignacionesEntity, long> asignacionesRepository)
        {
            _asignacionesRepository = asignacionesRepository;
        }
        public async Task<List<AsignacionesOutputDto>> ConsultaAsignaciones()
        {
            try
            {
                var consulta = await _asignacionesRepository.GetAll()
                                .Include(x => x.concepto)
                                .ToListAsync();
                return ObjectMapper.Map<List<AsignacionesOutputDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<AsignacionesOutputDto>> ConsultaAsignacionesPorUsuario(string codigo)
        {
            try
            {
                var consulta = await _asignacionesRepository.GetAll()
                                .Where(x => x.codigo == codigo)
                                .Include(x => x.concepto)

                                .ToListAsync();
                return ObjectMapper.Map<List<AsignacionesOutputDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

            public async Task<bool> EliminaAsignaciones(long id)
        {
            try
            {
                await _asignacionesRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraAsignaciones(AsignacionesInputDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<AsignacionesEntity>(input);
                var id = await _asignacionesRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
