﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.Asignaciones.Dto
{
    public class AsignacionesOutputDto
    {
        public long id { get; set; }
        public string nombre { get; set; }
        public string codigo { get; set; }
        public Conceptosentity concepto { get; set; }
        public long idConcepto { get; set; }
        public string descripcion { get; set; }
        public double valor { get; set; }
        public string estado { get; set; }
    }
}
