﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Asignaciones.Dto;

namespace webSysdatecback.Asignaciones
{
    public interface IAsignacionesAppService
    {
        Task<bool> RegistraAsignaciones(AsignacionesInputDto input);
        Task<List<AsignacionesOutputDto>> ConsultaAsignaciones();
        Task<List<AsignacionesOutputDto>> ConsultaAsignacionesPorUsuario(string codigo);
        Task<bool> EliminaAsignaciones(long id);
    }
}
