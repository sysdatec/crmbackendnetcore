﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Conceptos.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Conceptos
{
    public class ConceptosAppService : webSysdatecbackAppServiceBase, IConceptosAppService
    {
        private readonly IRepository<Conceptosentity, long> _conceptosRepository;
       

        public ConceptosAppService(IRepository<Conceptosentity, long> conceptosRepository)
        {
            _conceptosRepository = conceptosRepository;
        }
        public async Task<List<ConceptosDto>> ConsultarConcepto()
        {
            try
            {
                var consulta = await _conceptosRepository.GetAllListAsync();
                return ObjectMapper.Map<List<ConceptosDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminarConceptos(long id)
        {
            try
            {
                await _conceptosRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraActualizaConceptos(ConceptosDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<Conceptosentity>(input);
                var id = await _conceptosRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
