﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Conceptos.Dto
{
    public class ConceptosDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public string referenciaProducto { get; set; }
    }
}
