﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Conceptos.Dto;

namespace webSysdatecback.Conceptos
{
    public interface IConceptosAppService
    {
        Task<Boolean> RegistraActualizaConceptos(ConceptosDto input);
        Task<Boolean> EliminarConceptos(long id);
        Task<List<ConceptosDto>> ConsultarConcepto();

    }
}
