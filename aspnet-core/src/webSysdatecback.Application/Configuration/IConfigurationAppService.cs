﻿using System.Threading.Tasks;
using webSysdatecback.Configuration.Dto;

namespace webSysdatecback.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
