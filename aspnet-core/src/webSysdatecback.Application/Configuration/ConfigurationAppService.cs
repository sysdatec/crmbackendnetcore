﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using webSysdatecback.Configuration.Dto;

namespace webSysdatecback.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : webSysdatecbackAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
