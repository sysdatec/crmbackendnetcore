﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Entidades;
using webSysdatecback.Personas.PersonasDto;
using webSysdatecback.Users;
using webSysdatecback.Users.Dto;

namespace webSysdatecback.Personas
{
    public class PersonasAppService : webSysdatecbackAppServiceBase, IPersonasAppService
    {
        private readonly IRepository<PersonasEntity,long> _personasRepository;

        public PersonasAppService(IRepository<PersonasEntity, long> personasRepository)
        {
            _personasRepository = personasRepository;
        }
        public async Task<List<PersonasOutPutDto>> ConsultarPersonas()
        {
            try
            {
                var consulta = await _personasRepository.GetAll()
                                .Include(x => x.usuario)
                                .ToListAsync();
                return ObjectMapper.Map<List<PersonasOutPutDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminaPersona(long id)
        {
            try
            {
                await _personasRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraActualizaPersona(PersonasInputDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<PersonasEntity>(input);
                var id = await _personasRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        
    }
}
