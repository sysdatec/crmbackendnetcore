﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Personas.PersonasDto;

namespace webSysdatecback.Personas
{
    public interface IPersonasAppService
    {
        Task<Boolean> RegistraActualizaPersona(PersonasInputDto input);
        Task<List<PersonasOutPutDto>> ConsultarPersonas();
        Task<Boolean> EliminaPersona(long id);
    }
}
