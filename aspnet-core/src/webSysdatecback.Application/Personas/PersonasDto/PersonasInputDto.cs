﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Authorization.Users;

namespace webSysdatecback.Personas.PersonasDto
{
    public class PersonasInputDto
    {
        public long id { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
        public string empresa { get; set; }
        public string pais { get; set; }
        public string departamento { get; set; }
        public string ciudad { get; set; }
        //public User usuario { get; set; }
        public long IdUser { get; set; }
    }
}
