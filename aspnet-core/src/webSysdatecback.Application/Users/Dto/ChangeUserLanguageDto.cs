using System.ComponentModel.DataAnnotations;

namespace webSysdatecback.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}