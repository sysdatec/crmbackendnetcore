﻿using System.Threading.Tasks;
using Abp.Application.Services;
using webSysdatecback.Authorization.Accounts.Dto;

namespace webSysdatecback.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
