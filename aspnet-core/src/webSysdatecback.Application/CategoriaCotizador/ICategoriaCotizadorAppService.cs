﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.CategoriaCotizador.Dto;

namespace webSysdatecback.CategoriaCotizador
{
    public interface ICategoriaCotizadorAppService
    {
        Task<bool> RegistrarCategoria(CategoriaCotizadorInputDto input);
        Task<bool> EliminarCategoria(long id);
        Task<List<CategoriaCotizadorOutPutDto>> ConsultarCategoria();
    }
}
