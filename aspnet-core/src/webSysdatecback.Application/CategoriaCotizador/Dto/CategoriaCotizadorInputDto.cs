﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.CategoriaCotizador.Dto
{
    public class CategoriaCotizadorInputDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
    }
}
