﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.CategoriaCotizador.Dto
{
    public class CategoriaCotizadorOutPutDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
    }
}
