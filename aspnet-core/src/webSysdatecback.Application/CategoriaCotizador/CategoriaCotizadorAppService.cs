﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.CategoriaCotizador.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.CategoriaCotizador
{
    public class CategoriaCotizadorAppService : webSysdatecbackAppServiceBase, ICategoriaCotizadorAppService
    {
        private readonly IRepository<CategoriaCotizadorEntity, long> _categoriaCotizadorRepository;
        public CategoriaCotizadorAppService(IRepository<CategoriaCotizadorEntity, long> categoriaCotizadorRepository)
        {
            _categoriaCotizadorRepository = categoriaCotizadorRepository;
        }
        public async Task<List<CategoriaCotizadorOutPutDto>> ConsultarCategoria()
        {
            try
            {
                var entity = await _categoriaCotizadorRepository.GetAllListAsync();
                return ObjectMapper.Map<List<CategoriaCotizadorOutPutDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminarCategoria(long id)
        {
            try
            {
                await _categoriaCotizadorRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistrarCategoria(CategoriaCotizadorInputDto input)
        {
            try
            {
                bool respesta = false;
                var entity = ObjectMapper.Map<CategoriaCotizadorEntity>(input);
                var id = await _categoriaCotizadorRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respesta = true;
                return respesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
