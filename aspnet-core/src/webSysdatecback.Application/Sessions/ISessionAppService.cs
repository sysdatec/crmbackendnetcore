﻿using System.Threading.Tasks;
using Abp.Application.Services;
using webSysdatecback.Sessions.Dto;

namespace webSysdatecback.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
