﻿using Org.BouncyCastle.Crypto.Digests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.ProductoServicios.Dto;

namespace webSysdatecback.ProductoServicios
{
    public interface IProductosServiceAppService
    {
        Task<Boolean> RegistraActualizaProducto(ProductoServiceDto input);
        Task<Boolean> EliminaProdunto(long id);
        Task<List<ProductoServiceDto>> ConsultaProductos();
    }
}
