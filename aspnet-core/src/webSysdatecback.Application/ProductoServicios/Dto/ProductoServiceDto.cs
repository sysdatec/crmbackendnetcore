﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.ProductoServicios.Dto
{
    public class ProductoServiceDto
    {
        public long id { get; set; }
        public PersonasEntity persona { get; set; }
        public long IdPersona { get; set; }
        public Conceptosentity conceptos { get; set; }
        public long IdConcepto { get; set; }
        public double valor { get; set; }
    }
}
