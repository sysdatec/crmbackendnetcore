﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Crypto.Digests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Entidades;
using webSysdatecback.ProductoServicios.Dto;

namespace webSysdatecback.ProductoServicios
{
    class ProductosServiceAppService : webSysdatecbackAppServiceBase, IProductosServiceAppService
    {
        private readonly IRepository<ProductoServiciosEntity, long> _productoRepository;
        
        public ProductosServiceAppService(IRepository<ProductoServiciosEntity, long> productoRepository)
        {
            _productoRepository = productoRepository;
        }
        public async Task<List<ProductoServiceDto>> ConsultaProductos()
        {
            try
            {
                var consulta = await _productoRepository.GetAll()
                                .Include(x => x.persona)
                                .Include(x => x.conceptos)
                                .ToListAsync();
                return ObjectMapper.Map<List<ProductoServiceDto>>(consulta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminaProdunto(long id)
        {
            try
            {
                await _productoRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraActualizaProducto(ProductoServiceDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<ProductoServiciosEntity>(input);
                var id = await _productoRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
