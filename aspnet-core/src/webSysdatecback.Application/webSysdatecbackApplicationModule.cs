﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using webSysdatecback.Authorization;

namespace webSysdatecback
{
    [DependsOn(
        typeof(webSysdatecbackCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class webSysdatecbackApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<webSysdatecbackAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(webSysdatecbackApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
