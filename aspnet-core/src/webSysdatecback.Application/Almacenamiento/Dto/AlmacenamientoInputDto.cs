﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Almacenamiento.Dto
{
    public class AlmacenamientoInputDto
    {
        public long id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string capacidad { get; set; }
        public double valor { get; set; }
        public string tipoAlmacenamiento { get; set; }
    }
}
