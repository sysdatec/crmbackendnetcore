﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Almacenamiento.Dto;

namespace webSysdatecback.Almacenamiento
{
    public interface IAlmacenamientoAppService
    {
        Task<bool> RegistrarAlmacenamiento(AlmacenamientoInputDto input);
        Task<bool> EliminaAlmacenamiento(long id);
        Task<List<AlmacenamientoInputDto>> ConsultarAlmacenamiento();
        Task<List<AlmacenamientoInputDto>> ConsultarAlmacenamientoPorTipo(string tipo);
    }
}
