﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Almacenamiento.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Almacenamiento
{
    public class AlmacenamientoAppService : webSysdatecbackAppServiceBase, IAlmacenamientoAppService
    {
        private readonly IRepository<AlmacenamientoAdicionalEntity, long> _almacenamientoAdicionalsRepository;

        public AlmacenamientoAppService(IRepository<AlmacenamientoAdicionalEntity, long> almacenamientoAdicionalsRepository)
        {
            _almacenamientoAdicionalsRepository = almacenamientoAdicionalsRepository;
        }
        public async Task<List<AlmacenamientoInputDto>> ConsultarAlmacenamiento()
        {
            try
            {
                var entity = await _almacenamientoAdicionalsRepository.GetAllListAsync();
                return ObjectMapper.Map<List<AlmacenamientoInputDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<AlmacenamientoInputDto>> ConsultarAlmacenamientoPorTipo(string tipo)
        {
            try
            {
                var entity = await _almacenamientoAdicionalsRepository.GetAll().Where(x=>x.tipoAlmacenamiento==tipo).ToListAsync();
                return ObjectMapper.Map<List<AlmacenamientoInputDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminaAlmacenamiento(long id)
        {
            try
            {
                await _almacenamientoAdicionalsRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistrarAlmacenamiento(AlmacenamientoInputDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<AlmacenamientoAdicionalEntity>(input);
                var id = await _almacenamientoAdicionalsRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;
                return respuesta;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
