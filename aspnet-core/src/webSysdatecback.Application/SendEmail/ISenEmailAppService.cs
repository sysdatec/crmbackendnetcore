﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.SendEmail.Dto;

namespace webSysdatecback.SendEmail
{
    public interface ISenEmailAppService
    {
        Task EnviarCorreo(CorreoDto inputCorreo);
        Task SendEmail(EnviarCorreoDto input);
        Task EnviarCorreoMailkit();
    }
}
