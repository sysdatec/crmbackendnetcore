﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.SendEmail.Dto
{
    public class CorreoDto
    {
        public string nombreCompleto { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }
        public string nombreEmpresa { get; set; }
    }
}
