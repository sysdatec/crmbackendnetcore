﻿using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.SendEmail.Dto
{
    public class EnviarCorreoDto
    {
        public string email { get; set; }
        public string asunto { get; set; }
        public string mensaje { get; set; }
    }
}
