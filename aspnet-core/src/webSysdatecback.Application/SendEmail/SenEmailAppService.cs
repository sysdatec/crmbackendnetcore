﻿using Abp.Net.Mail;
using Abp.UI;
using Amazon.Runtime;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using webSysdatecback.SendEmail.Dto;

namespace webSysdatecback.SendEmail
{
    public class SenEmailAppService : webSysdatecbackAppServiceBase, ISenEmailAppService
    {
        private readonly IEmailSender _emailSender;
        private readonly IConfiguration _configuration = BuildConfiguration();

        public SenEmailAppService(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile("appsettings.Production.json", optional: true, reloadOnChange: true);
            return builder.Build();
        }
        public async Task EnviarCorreo(CorreoDto inputCorreo)
        {
            try
            {
                string emailOrigen = "elkinduranm@gmail.com";
                string emailDestino = "elkinduranm@gmail.com";
                string password = "$E880322";
                
                var client = new SmtpClient("smtp.gmail.com", 25)
                {
                    Credentials = new NetworkCredential(emailOrigen, password),
                    EnableSsl = true
                };
                client.Send(emailOrigen, emailDestino, "test", "testbody");
                /*Console.WriteLine("Sent");
                Console.ReadLine();*/
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }
        }

        public async  Task EnviarCorreoMailkit()
        {
            try
            {
                //Assign task to the person
                

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task SendEmail(EnviarCorreoDto input)
        {
            try
            {
                Console.WriteLine("inicia método sendEmail");
                var smtp = _configuration.GetValue<string>($"sendmail:smtp");
                if (smtp=="sendgrid")
                {
                    var apiKey = _configuration.GetValue<string>($"sendmail:token");
                    Console.WriteLine(apiKey);
                    var client = new SendGridClient(apiKey);
                    var from = new EmailAddress(_configuration.GetValue<string>($"sendmail:email"));
                    Console.WriteLine(from);
                    var to = new EmailAddress(input.email);
                    var plainTextContent = Regex.Replace(input.mensaje, "<[^>]*>", "");
                    var smg = MailHelper.CreateSingleEmail(from, to, input.asunto, plainTextContent, input.mensaje);
                    var response = await client.SendEmailAsync(smg);

                    Console.WriteLine(response.StatusCode);
                }
                else if(smtp=="smtp")
                {
                    var from = _configuration.GetValue<string>($"sendmail:email");
                    string password = _configuration.GetValue<string>($"sendmail:keyEmail");
                    int port = _configuration.GetValue<int>($"sendmail:portSmtp");


                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(from);
                    mail.To.Add(input.email);
                    mail.Subject = input.asunto;
                    var client = new SmtpClient("smtp.gmail.com", port)
                    {
                        Credentials = new NetworkCredential(from, password),
                        EnableSsl = true
                    };
                    mail.IsBodyHtml = true;
                    mail.Body = input.mensaje;
                    client.Send(mail);
                }
                else if (smtp== "awsses")
                {
                    //esta opción utiliza la api aws ses( simple email services) para envio de correo por amazon
                    using (var client = new AmazonSimpleEmailServiceClient(Amazon.RegionEndpoint.USEast1))
                    {
                        var sendRequest = new SendEmailRequest
                        {
                            Source = _configuration.GetValue<string>($"sendmail:email"),
                            Destination = new Destination { ToAddresses = { input.email } },
                            Message = new Message
                            {
                                Subject = new Amazon.SimpleEmail.Model.Content(input.asunto),
                                Body = new Body
                                {
                                    Html = new Amazon.SimpleEmail.Model.Content(input.mensaje)
                                }
                            }
                        };

                        try
                        {
                            var response = client.SendEmailAsync(sendRequest).Result;
                            Console.WriteLine("Email sent! Message ID = {0}", response.MessageId);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Send failed with exception: {0}", ex.Message);
                        }
                    }

                }
                else
                {
                    throw new UserFriendlyException($"Error: no hay configuración seleccionado en el appseting");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
