﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Descargas.Dto;

namespace webSysdatecback.Descargas
{
    public interface DesacargaAppService
    {
        Task<bool> RegistraDescarga(DescargaInputDto inputDto);
        Task<bool> EliminarDescarga(long id);
        Task<List<DescargaOutPut>> ConsultarDEscarga();

    }
}
