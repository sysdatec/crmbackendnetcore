﻿using System;
using System.Collections.Generic;
using System.Text;
using webSysdatecback.Entidades;

namespace webSysdatecback.Descargas.Dto
{
    public class DescargaOutPut
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public DateTime CreationTime { get; set; }
        public CategoriaEntity categoria { get; set; }
        public int IdCategoria { get; set; }
        public string descripcion { get; set; }
        public string pathVideo { get; set; }
        public string url { get; set; }
    }
}
