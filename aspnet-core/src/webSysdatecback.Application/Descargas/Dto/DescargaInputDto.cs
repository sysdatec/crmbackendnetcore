﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace webSysdatecback.Descargas.Dto
{
    public class DescargaInputDto
    {
        public long id { get; set; }
        public string titulo { get; set; }
        public int IdCategoria { get; set; }
        public string descripcion { get; set; }
        public IFormFile pathVideo { get; set; }
        public string url { get; set; }
    }
}
