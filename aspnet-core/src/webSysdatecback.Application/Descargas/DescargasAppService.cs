﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webSysdatecback.Descargas.Dto;
using webSysdatecback.Entidades;

namespace webSysdatecback.Descargas
{
    public class DescargasAppService: webSysdatecbackAppServiceBase, DesacargaAppService
    {
        private readonly IRepository<DescargasEntity, long> _descargasRepository;
        public DescargasAppService(IRepository<DescargasEntity, long> descargasRepository)
        {
            _descargasRepository = descargasRepository;
        }
        public async Task<List<DescargaOutPut>> ConsultarDEscarga()
        {
            try
            {
                var entity = await _descargasRepository.GetAll()
                            .Include(x => x.categoria).ToListAsync();
                return ObjectMapper.Map<List<DescargaOutPut>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> EliminarDescarga(long id)
        {
            try
            {
                await _descargasRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraDescarga(DescargaInputDto inputDto)
        {
            try
            {
                bool respuesta = false;
                /*var entity = ObjectMapper.Map<DescargasEntity>(inputDto);
                var id = await _descargasRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (id > 0) respuesta = true;*/
                return respuesta;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
