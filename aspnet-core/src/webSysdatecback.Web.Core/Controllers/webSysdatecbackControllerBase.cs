using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace webSysdatecback.Controllers
{
    public abstract class webSysdatecbackControllerBase: AbpController
    {
        protected webSysdatecbackControllerBase()
        {
            LocalizationSourceName = webSysdatecbackConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
