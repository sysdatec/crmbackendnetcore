﻿using System.Collections.Generic;

namespace webSysdatecback.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
